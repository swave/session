steps = [
    [
        # "Up" SQL Statement
        """
        CREATE TABLE venues (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(50) NOT NULL,
            city VARCHAR(50) NOT NULL,
            capacity INT NOT NULL,
            all_ages BOOL NOT NULL
            -- members TEXT NULL (model/VO/FK),
            -- details TEXT NULL (model/VO/FK),
        );
        """,
        # "Down" SQL Statement
        """
        DROP TABLE venues;
        """,
    ]
]
