from fastapi import APIRouter, Depends, HTTPException, Response, status
from typing import Union, List, Optional
from queries.bands import Error, BandRepository, BandIn, BandOut
from authenticator import authenticator

router = APIRouter()


@router.post("/bands", response_model=Union[BandOut, Error])
def create_band(
    band: BandIn,
    response: Response,
    repo: BandRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        return repo.create(band)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        ) from e


@router.get("/bands/{band_id}", response_model=Optional[BandOut])
def get_one_band(
    band_id: int,
    repo: BandRepository = Depends(),
) -> BandOut:
    try:
        return repo.get_one_band(band_id)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"{str(e)}",
        ) from e


@router.get("/bands", response_model=Union[List[BandOut], Error])
def get_all(
    repo: BandRepository = Depends(),
):
    try:
        return repo.get_all()
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Internal Server Error: {str(e)}",
        ) from e


@router.delete("/bands/{band_id}", response_model=bool)
def delete_band(
    band_id: int,
    response: Response,
    repo: BandRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    try:
        deleted_band = repo.delete(band_id)
        return deleted_band
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"{str(e)}",
        ) from e


@router.put("/bands/{band_id}", response_model=Union[BandOut, Error])
def update_band(
    band_id: int,
    band: BandIn,
    response: Response,
    repo: BandRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        updated_band = repo.update(band_id, band)
        return updated_band
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        ) from e
