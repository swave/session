from fastapi import APIRouter
from pydantic import BaseModel
from datetime import datetime

router = APIRouter()

conversations = []


class Message(BaseModel):
    sender: str
    recipient: str
    content: str
    timestamp: datetime


@router.post("/send_message/")
async def send_message(message: Message):
    conversations.append(message)
    return {"message": "Message sent successfully"}


@router.get("/get_messages/{user1}/{user2}/")
async def get_messages(user1: str, user2: str):
    conversation = [
        msg
        for msg in conversations
        if (msg.sender == user1 and msg.recipient == user2)
        or (msg.sender == user2 and msg.recipient == user1)
    ]
    return conversation


@router.get("/poll_messages/")
async def poll_messages(user1: str, user2: str, timestamp: datetime):
    new_messages = [
        msg
        for msg in conversations
        if (
            (msg.sender == user1 and msg.recipient == user2)
            or (msg.sender == user2 and msg.recipient == user1)
        )
        and msg.timestamp > timestamp
    ]
    return new_messages
