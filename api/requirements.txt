fastapi[all]>=0.78.0 #0.81.0
sqlalchemy>=1.4.32
uvicorn[standard]>=0.17.6
pytest
psycopg[binary,pool]>=3.1, <3.2
jwtdown-fastapi>=0.2.0
websockets>=12.0
