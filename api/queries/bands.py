from pydantic import BaseModel
from queries.pool import pool
from typing import Optional


class Error(BaseModel):
    message: str


class BandIn(BaseModel):
    username: str
    name: str
    email: str
    photo: str


class BandOut(BaseModel):
    id: int
    username: str
    name: str
    email: str
    photo: str


class BandRepository:
    def get_one_band(self, band_id: int) -> Optional[BandOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT * FROM bands
                    WHERE id = %s
                    """,
                    [band_id],
                )
                result = db.fetchone()
                if result is None:
                    raise Exception("Band not found")
                result = db.execute(
                    """
                    SELECT id
                        , username
                        , name
                        , email
                        , photo
                    FROM bands
                    WHERE id = %s
                    """,
                    [band_id],
                )
                record = result.fetchone()
                return self.record_to_band_out(record)

    def create(self, band: BandIn) -> BandOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO bands
                        (username, name, email, photo)
                    VALUES
                        (%s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        band.username,
                        band.name,
                        band.email,
                        band.photo,
                    ],
                )
                id = result.fetchone()[0]
                old_data = band.dict()
                return BandOut(id=id, **old_data)

    def get_all(self):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            id, username, name, email, photo
                        FROM bands
                        ORDER BY id
                        """
                    )
                    return [
                        BandOut(
                            id=record[0],
                            username=record[1],
                            name=record[2],
                            email=record[3],
                            photo=record[4],
                        )
                        for record in db
                    ]
        except Exception:
            return {"Message": "Could not get all bands"}

    def delete(self, band_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT * FROM bands
                    WHERE id = %s
                    """,
                    [band_id],
                )
                result = db.fetchone()
                if result is None:
                    raise Exception("Band not found")

                db.execute(
                    """
                    DELETE FROM bands
                    WHERE id = %s
                    """,
                    [band_id],
                )
                return True

    def update(self, band_id: int, band: BandIn) -> BandOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT * FROM bands
                    WHERE id = %s
                    """,
                    [band_id],
                )
                result = db.fetchone()
                if result is None:
                    raise Exception("Band not found")

                db.execute(
                    """
                    UPDATE bands
                    SET
                        username = %s,
                        name = %s,
                        email = %s,
                        photo = %s
                    WHERE id = %s
                    """,
                    [
                        band.username,
                        band.name,
                        band.email,
                        band.photo,
                        band_id,
                    ],
                )
        return BandOut(id=band_id, **band.dict())

    def record_to_band_out(self, record):
        return BandOut(
            id=record[0],
            username=record[1],
            name=record[2],
            email=record[3],
            photo=record[4],
        )
