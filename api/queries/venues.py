from pydantic import BaseModel
from queries.pool import pool
from typing import Union, Optional


class Error(BaseModel):
    message: str


class VenueIn(BaseModel):
    name: str
    city: str
    capacity: int
    all_ages: bool
    photo: str


class VenueOut(BaseModel):
    id: int
    name: str
    city: str
    capacity: int
    all_ages: bool
    photo: str


class VenueRepository:
    def get_one(self, venue_id: int) -> Optional[VenueOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT * FROM venues
                    WHERE id = %s
                    """,
                    [venue_id],
                )
                result = db.fetchone()
                if result is None:
                    raise Exception("Venue not found")

                result = db.execute(
                    """
                    SELECT id
                        , name
                        , city
                        , capacity
                        , all_ages
                        , photo
                    FROM venues
                    WHERE id = %s
                    """,
                    [venue_id],
                )
                record = result.fetchone()
                return self.record_to_venue_out(record)

    def update(self, venue_id: int, venue: VenueIn) -> Union[VenueOut, Error]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT * FROM venues
                    WHERE id = %s
                    """,
                    [venue_id],
                )
                result = db.fetchone()
                if result is None:
                    raise Exception("Venue not found")

                db.execute(
                    """
                    UPDATE venues
                    SET name = %s
                    , city = %s
                    , capacity = %s
                    , all_ages = %s
                    , photo = %s
                    WHERE id = %s
                    """,
                    [
                        venue.name,
                        venue.city,
                        venue.capacity,
                        venue.all_ages,
                        venue.photo,
                        venue_id,
                    ],
                )
                return self.venue_in_to_out(venue_id, venue)

    def create(self, venue: VenueIn) -> VenueOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO venues
                        (name, city, capacity, all_ages, photo)
                    VALUES
                        (%s, %s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        venue.name,
                        venue.city,
                        venue.capacity,
                        venue.all_ages,
                        venue.photo,
                    ],
                )
                id = result.fetchone()[0]
                old_data = venue.dict()
                return VenueOut(id=id, **old_data)

    def get_all(self):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            id, name, city, capacity, all_ages, photo
                        FROM venues
                        ORDER BY id
                        """
                    )
                    return [
                        VenueOut(
                            id=record[0],
                            name=record[1],
                            city=record[2],
                            capacity=record[3],
                            all_ages=record[4],
                            photo=record[5],
                        )
                        for record in db
                    ]
        except Exception:
            return {"Message": "Could not get all venues"}

    def delete(self, venue_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT * FROM venues
                    WHERE id = %s
                    """,
                    [venue_id],
                )
                result = db.fetchone()
                print("RESULT", result)
                if result is None:
                    raise Exception("Venue not found")

                db.execute(
                    """
                    DELETE FROM venues
                    WHERE id = %s
                    """,
                    [venue_id],
                )
                return True

    def venue_in_to_out(self, id: int, venue: VenueIn):
        old_data = venue.dict()
        return VenueOut(id=id, **old_data)

    def record_to_venue_out(self, record):
        return VenueOut(
            id=record[0],
            name=record[1],
            city=record[2],
            capacity=record[3],
            all_ages=record[4],
            photo=record[5],
        )
