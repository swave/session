## Project Documentation

Please put any and all documentation for your project in this folder. Other than the documents that are required at the end of week 13, feel free to organize this however your group sees fit.

saved stuff

Creating deployed back end database
    glv-cloud-cli deploy -a sessiondb -i postgres:14.7 -e POSTGRES_DB=session_db -e POSTGRES_USER=session_admin -e POSTGRES_PASSWORD=session -m /var/lib/postgresql -p 5432 -x=false

Creating deployed back end api
    glv-cloud-cli deploy -a sessionapi -i registry.gitlab.com/the-wiggles/session/api:latest -e SIGNING_KEY=FFWoAg1IAf3+JLp7yD1FAv+6ijLsRTZ6S5xBwjTri8o= -e DATABASE_URL=postgresql://session_admin:session@nov-2023-4-sessiondb-service.default.svc.cluster.local/postgres -e CORS_HOST=https://the-wiggles.gitlab.io

    -e VITE_API_HOST = https://nov-2023-4-sessionapi.mod3projects.com

Resest after merging to main
    glv-cloud-cli reset -a sessionapi

Website link
    https://nov-2023-4-sessionapi.mod3projects.com/docs
    https://nov-2023-4-sessionapi.mod3projects.com
    https://the-wiggles.gitlab.io/session
