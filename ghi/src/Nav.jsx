import { NavLink } from 'react-router-dom';
import useToken from '@galvanize-inc/jwtdown-for-react';
import { useState, useEffect, useRef } from 'react'
import './App.css'


function Nav() {
    const { token, logout } = useToken()
    const handleLogout = async () => {
        logout()
    };

    const [isOpen, setIsOpen] = useState(false)
    const dropdownRef = useRef(null)

    useEffect(() => {
        function handleClickOutside(event) {
            if (
                dropdownRef.current &&
                !dropdownRef.current.contains(event.target)
            ) {
                setIsOpen(false)
            }
        }

        document.addEventListener('mousedown', handleClickOutside)
        return () => {
            document.removeEventListener('mousedown', handleClickOutside)
        }
    }, [])

    return (
        <nav className="bg-green-800 navbar-height">
            <div className="mx-auto max-w-8xl px-2 sm:px-6 lg:px-8">
                <div className="relative flex h-16 items-center justify-between">
                    <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                        <button
                            type="button"
                            className="relative inline-flex items-center justify-center rounded-md p-2 text-white-400 hover:bg-green-700 hover:text-white focus:outline-none focus:ring-2 focus:ring-white"
                            aria-controls="mobile-menu"
                            aria-expanded="false"
                        >
                            <span className="sr-only">Open main menu</span>
                            <svg
                                className="block h-6 w-6"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth="1.5"
                                stroke="currentColor"
                                aria-hidden="true"
                            >
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
                                />
                            </svg>
                            <svg
                                className="hidden h-6 w-6"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth="1.5"
                                stroke="currentColor"
                                aria-hidden="true"
                            >
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M6 18L18 6M6 6l12 12"
                                />
                            </svg>
                        </button>
                    </div>
                    <div className="flex flex-1 items-center justify-between sm:items-stretch sm:justify-start">
                        <div className="flex flex-shrink-0 items-center">
                            <img
                                className="flex h-32 w-auto"
                                style={{
                                    display: 'flex',
                                    justifyContent: 'right',
                                    alignItems: 'center',
                                    marginTop: '50px',
                                    marginLeft: '-25px',
                                }}
                                src="https://i.imgur.com/SgMPUzS.png"
                                alt="Your Company"
                            />
                        </div>
                        <h1
                            className="text-white text-4xl py-5"
                            style={{
                                display: 'flex',
                                justifyContent: 'right',
                                alignItems: 'center',
                                marginTop: '75px',
                                marginLeft: '-50px',
                            }}
                        >
                            {' '}
                            ession{' '}
                        </h1>
                        <div className="hidden sm:ml-10 sm:block sm:flex-grow">
                            <div className="flex items-center justify-center space-x-300 pt-10">
                                <NavLink
                                    className="text-white hover:bg-green-600 hover:text-white rounded-md px-5 py-3 text-4xl font-sm mr-60 border-b-2 border-white"
                                    to="/"
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        marginTop: '20px',
                                        marginLeft: '-75px',
                                    }}
                                >
                                    Jam
                                </NavLink>
                                <NavLink
                                    className="text-white hover:bg-green-600 hover:text-white rounded-md px-5 py-3 text-4xl font-sm mr-60 border-b-2 border-white"
                                    to="/bands"
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        marginTop: '20px',
                                        marginLeft: '-75px',
                                    }}
                                >
                                    Lineup
                                </NavLink>
                                <NavLink
                                    className="text-white hover:bg-green-600 hover:text-white rounded-md px-5 py-3 text-4xl font-sm mr-96 border-b-2 border-white"
                                    to="/venues"
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        marginTop: '20px',
                                        marginLeft: '-75px',
                                    }}
                                >
                                    Venues
                                </NavLink>
                            </div>
                            <div
                                className="flex items-center justify-end pt-2"
                                style={{
                                    display: 'flex',
                                    justifyContent: 'right',
                                    alignItems: 'center',
                                    marginTop: '-50px',
                                }}
                            >
                                <div
                                    ref={dropdownRef}
                                    className="relative inline-block text-left"
                                >
                                    <button
                                        type="button"
                                        className="inline-flex justify-center w-full rounded-md px-4 py-2 bg-white text-2xl font-bold text-gray-700 hover:bg-gray-50"
                                        id="options-menu"
                                        aria-haspopup="true"
                                        aria-expanded={isOpen}
                                        onClick={() => setIsOpen(!isOpen)}
                                    >
                                        Account
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            stroke="currentColor"
                                            className="-mr-12 ml-2 h-10 w-32"
                                        >
                                            <rect
                                                width="24"
                                                height="24"
                                                stroke="currentColor"
                                                fill="none"
                                            />
                                            <path
                                                strokeLinecap="round"
                                                strokeLinejoin="round"
                                                strokeWidth={2}
                                                d="M4 6h16M4 12h16M4 18h16"
                                            />
                                        </svg>
                                    </button>
                                    {isOpen && (
                                        <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                                            <div
                                                className="py-1"
                                                role="menu"
                                                aria-orientation="vertical"
                                                aria-labelledby="options-menu"
                                            >
                                                <NavLink
                                                    className="text-gray-700 block px-4 py-2 text-sm"
                                                    to="/users/myprofile"
                                                    role="menuitem"
                                                >
                                                    My Profile
                                                </NavLink>
                                                <NavLink
                                                    className="text-gray-700 block px-4 py-2 text-sm"
                                                    to="/venues/register"
                                                    role="menuitem"
                                                >
                                                    Register Venue
                                                </NavLink>
                                                <NavLink
                                                    className="text-gray-700 block px-4 py-2 text-sm"
                                                    to="/bands/register"
                                                    role="menuitem"
                                                >
                                                    Register Band
                                                </NavLink>
                                                {token ? (
                                                    <button
                                                        onClick={handleLogout}
                                                        className="text-gray-700 block px-4 py-2 text-sm cursor-pointer"
                                                        role="menuitem"
                                                    >
                                                        Logout
                                                    </button>
                                                ) : (
                                                    <>
                                                        <NavLink
                                                            className="text-gray-700 block px-4 py-2 text-sm"
                                                            to="/users/login"
                                                            role="menuitem"
                                                        >
                                                            Login
                                                        </NavLink>
                                                        <NavLink
                                                            className="text-gray-700 block px-4 py-2 text-sm"
                                                            to="/users/signup"
                                                            role="menuitem"
                                                        >
                                                            Sign Up
                                                        </NavLink>
                                                    </>
                                                )}
                                            </div>
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default Nav
