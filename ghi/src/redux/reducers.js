import { GET_USER_DATA_SUCCESS, UPDATE_USER_SUCCESS } from './actions';

const initialState = {
    users: {
        first_name: '',
        lastName: '',
        username: '',
        email: '',
    }
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_USER_DATA_SUCCESS:
        case UPDATE_USER_SUCCESS:
            return { ...state, user: action.payload };
        default:
            return state;
    }
};

export default userReducer;
