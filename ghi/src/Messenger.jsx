import { useState, useEffect, useContext } from 'react';
import{ UserContext } from './Users/UserContext';
import './App.css';
import useToken from "@galvanize-inc/jwtdown-for-react";
import { API_HOST } from './constants';

const Messenger = ({ receiverUsername }) => {
    const { loggedInUser } = useContext(UserContext);
    const { token } = useToken();
    const [messages, setMessages] = useState([]);
    const [messageInput, setMessageInput] = useState('');

    useEffect(() => {
        if (!loggedInUser || !token) {
            console.error('Current user or token is not available');
            return;
        }

        const fetchMessages = async () => {
            try {
                const response = await fetch(`${API_HOST}/get_messages/${loggedInUser}/${receiverUsername}/`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });

                if (response.ok) {
                    const data = await response.json()
                    setMessages(data)
                } else {
                    console.error(
                        'Failed to fetch messages:',
                        response.statusText
                    )
                }
            } catch (error) {
                console.error('Failed to fetch messages:', error)
            }
        }

        fetchMessages();

        const intervalId = setInterval(fetchMessages, 5000);


        return () => clearInterval(intervalId);
    }, [loggedInUser, token, receiverUsername]);

    const sendMessage = async () => {
        if (messageInput.trim() !== '') {
            const newMessage = {
                sender: loggedInUser,
                recipient: receiverUsername,
                content: messageInput,
                timestamp: new Date().toISOString(),
            };

            try {
                const response = await fetch(`${API_HOST}/send_message/`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify(newMessage),
                });

                if (response.ok) {
                    setMessageInput('');
                } else {
                    console.error('Failed to send message:', response.statusText);
                }
            } catch (error) {
                console.error('Failed to send message:', error)
            }
            console.log(newMessage)
        }
    }

    return (
        <div className="max-w-lg mx-auto bg-gray-100 p-4 rounded-lg h-full">
            <h2 className="text-xl font-semibold mb-4">Chat with {receiverUsername}</h2>
            <div className="chat-box h-96 overflow-y-auto">
                {messages.map((message, index) => (
                    <div key={index} className={`mb-2 ${message.sender === loggedInUser ? 'text-right' : 'text-left'}`}>
                        <p>{message.sender === loggedInUser ? 'You' : receiverUsername}: {message.content}</p>
                    </div>
                ))}
            </div>
            <div className="flex items-center mt-2">
                <input
                    type="text"
                    placeholder="Type your message..."
                    className="border border-gray-300 rounded-l p-2 w-full align-middle"
                    value={messageInput}
                    onChange={(e) => setMessageInput(e.target.value)}
                    onKeyDown={(e) => {
                        if (e.key === 'Enter') {
                            sendMessage();
                        }
                    }}
                />
                <div className="w-[2px]"></div>
                <button onClick={sendMessage} className="bg-lime-500 text-white px-4 py-[calc(0.5rem+0.5px)] rounded-r align-middle ml-1">
                    Send
                </button>
            </div>
        </div>
    )
}

export default Messenger;
