import { useState } from 'react'
import '../App.css'
import { useDispatch, useSelector } from 'react-redux'
import {
    registrationRequest,
    registrationSuccess,
    registrationFailure,
    selectRegistrationLoading,
    selectRegistrationError,
} from '../registrationSlice'
import { API_HOST } from '../constants'

const SignUpForm = () => {
    const dispatch = useDispatch()
    const loading = useSelector(selectRegistrationLoading)
    const error = useSelector(selectRegistrationError)

    const [username, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [photo, setPhoto] = useState(
        'https://i.pinimg.com/474x/f1/da/a7/f1daa70c9e3343cebd66ac2342d5be3f.jpg'
    )
    const genres = []
    const instruments = []
    const handleSubmit = async (e) => {
        e.preventDefault()

        if (password !== confirmPassword) {
            dispatch(registrationFailure("Passwords don't match"))
            return
        }

        dispatch(registrationRequest())

        try {
            const response = await fetch(`${API_HOST}/api/users`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username,
                    password,
                    first_name,
                    last_name,
                    email,
                    photo,
                    genres,
                    instruments,
                }),
            })

            if (response.ok) {
                dispatch(registrationSuccess())
                console.log('User successfully registered!')
                setUsername('')
                setEmail('')
                setPassword('')
                setConfirmPassword('')
                setFirstName('')
                setLastName('')
                setPhoto('')
            } else {
                const errorMessage = await response.text()
                dispatch(registrationFailure(errorMessage))
                console.error('Failed to register user:', errorMessage)
            }
        } catch (error) {
            dispatch(
                registrationFailure(
                    'An unexpected error occurred. Please try again later.'
                )
            )
            console.error('An unexpected error occurred:', error)
        }
    }

    return (
        <div className="flex items-center justify-center h-screen">
            <div className="max-w-md p-6 bg-white rounded shadow-md text-center">
                <h1 className=" text-3xl mb-4 font-bold">Sign Up</h1>
                <form onSubmit={handleSubmit}>
                    <div className="mb-4">
                        <label
                            htmlFor="username"
                            className="block text-gray-600"
                        >
                            Username:
                        </label>
                        <input
                            type="text"
                            id="username"
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}
                            required
                            className="w-full p-2 border rounded mt-1"
                        />
                    </div>
                    <div className="mb-4">
                        <label
                            htmlFor="password"
                            className="block text-gray-600"
                        >
                            Password:
                        </label>
                        <input
                            type="password"
                            id="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                            className="w-full p-2 border rounded mt-1"
                        />
                    </div>
                    <div className="mb-4">
                        <label
                            htmlFor="confirmPassword"
                            className="block text-gray-600"
                        >
                            Confirm Password:
                        </label>
                        <input
                            type="password"
                            id="confirmPassword"
                            value={confirmPassword}
                            onChange={(e) => setConfirmPassword(e.target.value)}
                            required
                            className="w-full p-2 border rounded mt-1"
                        />
                    </div>
                    <div className="mb-4">
                        <label
                            htmlFor="first_name"
                            className="block text-gray-600"
                        >
                            First Name:
                        </label>
                        <input
                            type="text"
                            id="first_name"
                            value={first_name}
                            onChange={(e) => setFirstName(e.target.value)}
                            required
                            className="w-full p-2 border rounded mt-1"
                        />
                    </div>
                    <div className="mb-4">
                        <label
                            htmlFor="last_name"
                            className="block text-gray-600"
                        >
                            Last Name:
                        </label>
                        <input
                            type="text"
                            id="last_name"
                            value={last_name}
                            onChange={(e) => setLastName(e.target.value)}
                            required
                            className="w-full p-2 border rounded mt-1"
                        />
                    </div>
                    <div className="mb-4">
                        <label htmlFor="email" className="block text-gray-600">
                            Email:
                        </label>
                        <input
                            type="email"
                            id="email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            required
                            className="w-full p-2 border rounded mt-1"
                        />
                    </div>
                    <div className="mb-4">
                        <label htmlFor="photo" className="block text-gray-600">
                            Photo:
                        </label>
                        <input
                            type="text"
                            id="photo"
                            value={photo}
                            onChange={(e) => setPhoto(e.target.value)}
                            className="w-full p-2 border rounded mt-1"
                        />
                    </div>

                    {error && <p className="text-red-500 mb-4">{error}</p>}

                    <div>
                        <button
                            type="submit"
                            className="w-full bg-blue-500 text-white p-2 rounded"
                            disabled={loading}
                        >
                            {loading ? 'Signing Up...' : 'Sign Up'}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default SignUpForm
