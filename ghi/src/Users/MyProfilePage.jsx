import { useState, useEffect, useContext } from 'react'
import { UserContext } from './UserContext'
import { API_HOST } from '../constants'
import '../App.css'
import useToken from '@galvanize-inc/jwtdown-for-react'
import { NavLink } from 'react-router-dom'

const MyProfilePage = () => {
    const { loggedInUser } = useContext(UserContext)
    const [userData, setUserData] = useState()
    const [editing, setEditing] = useState(false)
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        email: '',
        photo: '',
        genres: [],
        instruments: [],
        username: '',
        password: '',
    })

    const { token } = useToken()

    useEffect(() => {
        const fetchUserData = async () => {
            console.log(loggedInUser)
            try {
                const response = await fetch(
                    `${API_HOST}/users/${loggedInUser}`
                )
                if (!response.ok) {
                    throw new Error(
                        `Failed to fetch user data: ${response.status} ${response.statusText}`
                    )
                }
                const userData = await response.json()
                setUserData(userData)
                setFormData({
                    first_name: userData.first_name,
                    last_name: userData.last_name,
                    email: userData.email,
                    photo: userData.photo || '',
                    genres: userData.genres || [],
                    instruments: userData.instruments || [],
                    username: userData.username,
                    password: '',
                })
            } catch (error) {
                console.error('Error fetching user data:', error)
            }
        }

        if (loggedInUser) {
            fetchUserData()
        }
    }, [loggedInUser])

    const handleEditToggle = () => {
        setEditing(!editing)
    }

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const response = await fetch(`${API_HOST}/users/${loggedInUser}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify(formData),
            })
            if (!response.ok) {
                const errorMessage = await response.text()
                throw new Error(
                    `Failed to update user data: ${response.status} ${response.statusText}. Server response: ${errorMessage}`
                )
            }
            console.log('User data updated successfully!')
        } catch (error) {
            console.error('Error updating user data:', error)
        }
    }

    if (!userData) {
        return <div>Loading...</div>
    }

    return (
        <div className="max-w-800 mx-auto bg-white p-8 rounded shadow-md mt-10">
            <h1 className="text-3xl font-semibold mb-4">
                {loggedInUser}&apos;s Profile
            </h1>
            {editing ? (
                <form onSubmit={handleSubmit}>
                    <div className="mb-4">
                        <label className="font-semibold" htmlFor="first_name">
                            First Name:
                        </label>
                        <input
                            className="px-1 border border-slate-800 rounded"
                            type="text"
                            id="first_name"
                            name="first_name"
                            value={formData.first_name}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="mb-4">
                        <label className="font-semibold" htmlFor="last_name">
                            Last Name:
                        </label>
                        <input
                            className="px-1 border border-slate-800 rounded"
                            type="text"
                            id="last_name"
                            name="last_name"
                            value={formData.last_name}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="mb-4">
                        <label className="font-semibold" htmlFor="email">
                            Email:
                        </label>
                        <input
                            className="px-1 border border-slate-800 rounded"
                            type="email"
                            id="email"
                            name="email"
                            value={formData.email}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="mb-4">
                        <label className="font-semibold" htmlFor="photo">
                            Photo Url:
                        </label>
                        <input
                            className="px-1 border border-slate-800 rounded"
                            type="text"
                            id="photo"
                            name="photo"
                            value={formData.photo}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="mb-4">
                        <label className="font-semibold" htmlFor="username">
                            Username:
                        </label>
                        <input
                            className="px-1 border border-slate-800 rounded"
                            type="text"
                            id="username"
                            name="username"
                            value={formData.username}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="mb-4">
                        <label className="font-semibold" htmlFor="password">
                            Password:
                        </label>
                        <input
                            className="px-1 border border-slate-800 rounded"
                            type="password"
                            id="password"
                            name="password"
                            value={formData.password}
                            onChange={handleChange}
                        />
                    </div>
                    <button
                        className="bg-green-600 text-white px-4 py-2 rounded mt-2"
                        type="submit"
                    >
                        Save Changes
                    </button>
                </form>
            ) : (
                <>
                    <div className="mb-4">
                        <p className="font-bold"> Username: </p>
                        <p>{userData.username}</p>
                    </div>
                    <div className="mb-4">
                        <p className="font-semibold">First Name:</p>
                        <p>{userData.first_name}</p>
                    </div>
                    <div className="mb-4">
                        <p className="font-semibold">Last Name:</p>
                        <p>{userData.last_name}</p>
                    </div>
                    <div className="mb-4">
                        <p className="font-semibold">Email:</p>
                        <p>{userData.email}</p>
                    </div>
                    <div className="mb-4">
                        <p className="font-semibold">Photo:</p>
                        {userData.photo ? (
                            <img
                                src={userData.photo}
                                alt="User Photo"
                                style={{ maxWidth: '200px' }}
                            />
                        ) : (
                            <p>No photo available</p>
                        )}
                    </div>
                    <div className="mb-4">
                        <p className="font-semibold">Genres:</p>
                        <p>{userData.genres.join(', ')}</p>
                    </div>

                    <div className="mb-4">
                        <p className="font-semibold">Instruments:</p>
                        <p>{userData.instruments.join(', ')}</p>
                    </div>
                    <div className="px-4 flex space-x-3">
                        <NavLink to="/users/myprofile/genres">
                            <button className="px-3 bg-green-600 text-white px-4 py-2 rounded mt-2">
                                Add Genre
                            </button>
                        </NavLink>
                        <NavLink to="/users/myprofile/instruments">
                            <button className="px-3 bg-green-600 text-white px-4 py-2 rounded mt-2">
                                Add Instrument
                            </button>
                        </NavLink>
                        <button
                            className="px-3 bg-green-600 text-white px-4 py-2 rounded mt-2"
                            onClick={handleEditToggle}
                        >
                            Edit Profile
                        </button>
                    </div>
                </>
            )}
        </div>
    )
}

export default MyProfilePage
