import { useState, useEffect, useContext } from 'react'
import { UserContext } from './UserContext'
import { API_HOST } from '../constants'
import '../App.css'
import GenreList from '../GenreList'

const AddGenreForm = ({ token }) => {
    const { loggedInUser } = useContext(UserContext)
    const [userData, setUserData] = useState()
    const [selectedGenre, setSelectedGenre] = useState('')
    const [error, setError] = useState(null)

    useEffect(() => {
        const fetchUserData = async () => {
            console.log(loggedInUser)
            try {
                const response = await fetch(
                    `${API_HOST}/users/${loggedInUser}`
                )
                if (!response.ok) {
                    throw new Error(
                        `Failed to fetch user data: ${response.status} ${response.statusText}`
                    )
                }
                const userData = await response.json()
                setUserData(userData)
            } catch (error) {
                console.error('Error fetching user data:', error)
                setError('Failed to fetch user data. Please try again.')
            }
        }

        if (loggedInUser) {
            fetchUserData()
        }
    }, [loggedInUser])

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const response = await fetch(
                `${API_HOST}/users/${loggedInUser}/genres`,
                {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify([selectedGenre]),
                }
            )

            if (!response.ok) {
                throw new Error(
                    `Failed to add genre: ${response.status} ${response.statusText}`
                )
            }
            console.log('Genre added successfully!')
        } catch (error) {
            console.error('Error adding genre:', error)
            setError('Failed to add genre. Please try again.')
        }
    }

    if (!userData) {
        return <div>Loading...</div>
    }

    return (
        <div>
            <h1>Add Genre</h1>
            {error && <p>{error}</p>}
            <form onSubmit={handleSubmit}>
                <label>Select Genre:</label>
                <select
                    value={selectedGenre}
                    onChange={(e) => setSelectedGenre(e.target.value)}
                >
                    <option value="">-- Select a genre --</option>
                    {GenreList.map((genre) => (
                        <option key={genre} value={genre}>
                            {genre}
                        </option>
                    ))}
                </select>
                <button type="submit">Add Genre</button>
            </form>
        </div>
    )
}

export default AddGenreForm
