import { useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router-dom';
import '../App.css';
import { UserContext } from './UserContext';
import { API_HOST } from '../constants';
import Messenger from '../Messenger';


const UserDetailPage = () => {
    const { loggedInUser } = useContext(UserContext);
    const { username } = useParams();
    const [user, setUser] = useState(null);
    const [messages, setMessages] = useState([]);


    useEffect(() => {
    const fetchMessages = async () => {
        try {
            const response = await fetch(`${API_HOST}/poll_messages/?user1=${loggedInUser}&user2=${username}`);
            if (response.ok) {
                const data = await response.json();
                setMessages(data);
            } else {
                console.error('Failed to fetch messages:', response.statusText);
            }
        } catch (error) {
            console.error('Failed to fetch messages:', error);
        }
    };

    if (loggedInUser) {
        fetch(`${API_HOST}/users/${username}`)
            .then(response => response.json())
            .then(data => setUser(data))
            .catch(error => console.error('Error fetching user:', error));
    }

    const intervalId = loggedInUser && setInterval(fetchMessages, 5000);

    return () => {
        clearInterval(intervalId);
    };
}, [username, loggedInUser]);


    if (!user) {
        return <div>Loading...</div>
    }

    return (
        <div className="max-w-800 mx-auto bg-white p-8 rounded shadow-md mt-10 flex">
            <div className="w-1/2 mr-5">
                <div className="flex items-center mb-4">
                    <img src={user.photo} alt="Profile" className="w-36 h-36 object-cover rounded-full" />
                    <div>
                        <h1 className="text-3xl px-2 font-semibold">
                            {user.first_name} {user.last_name}
                        </h1>
                        <p className="text-2xl px-2 font-semibold">@{user.username}</p>
                    </div>
                </div>
                <div className="w-full flex justify-between">
                    <div className="w-1/2">
                        <h3 className="text-l px-2 font-semibold">Genres:</h3>
                        <ul className="list-disc list-inside">
                            {user.genres.map((genre, index) => (
                                <li key={index} className="px-2 overflow-hidden whitespace-nowrap overflow-ellipsis">{genre}</li>
                            ))}
                        </ul>
                    </div>
                    <div className="w-1/2 flex flex-col justify-start">
                        <h3 className="text-l px-2 font-semibold">Instruments:</h3>
                        <ul className="list-disc list-inside">
                            {user.instruments.map((instrument, index) => (
                                <li key={index} className="px-2 overflow-hidden whitespace-nowrap overflow-ellipsis">{instrument}</li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
            <div className="w-1/2">
                 <Messenger receiverUsername={username} messages={messages} />
            </div>
        </div>
    );
};

export default UserDetailPage;
