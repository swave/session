import { API_HOST } from '../constants'
import { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom'
import '../App.css'

function Jam() {
    const [users, setUsers] = useState([])
    const [searchTerm, setSearchTerm] = useState('')

    useEffect(() => {
        fetch(`${API_HOST}/users`)
            .then((response) => response.json())
            .then((data) => setUsers(data))
            .catch((error) => console.error('Error fetching users:', error))
    }, [])

    if (users.length === 0) {
        return <div>Loading Users...</div>
    }

    const filteredUsers = users.filter((user) =>
        searchTerm
            .toLowerCase()
            .split(' ')
            .every(
                (term) =>
                    user.username.toLowerCase().includes(term) ||
                    user.genres.join(' ').toLowerCase().includes(term) ||
                    user.instruments.join(' ').toLowerCase().includes(term)
            )
    )

    return (
        <div className="container mx-auto mt-8 flex justify-center">
            <div className="w-3/4 p-4 flex flex-wrap">
                <h1 className="text-3xl font-semibold mb-4 w-full">
                    Jam Session Users:
                </h1>
                    <input
                        type="text"
                        placeholder="Genres, Instruments, Users..."
                        value={searchTerm}
                        onChange={(e) => setSearchTerm(e.target.value)}
                        className="w-full p-2 border bg-slate-900 border-slate-800 text-white rounded"
                    />
                {filteredUsers.map((user) => (
                    <NavLink
                        key={user.id}
                        to={`/users/profile/${user.username}`}
                        className="w-1/3 px-2 py-2 mb-4 my-2"
                    >
                        <div>
                            <img
                                src={user.photo}
                                alt="Profile"
                                className="w-full h-40 object-cover rounded-t"
                            />
                            <div className="bg-white rounded shadow-xl">
                                <h2 className="text-xl bg-green-900 font-semibold text-white px-2 py-2">
                                    {user.username}
                                </h2>
                            </div>
                        </div>
                    </NavLink>
                ))}
                {filteredUsers.length === 0 && (
                    <p className="text-gray-600 w-full">No users found.</p>
                )}
            </div>
        </div>
    )
}

export default Jam
