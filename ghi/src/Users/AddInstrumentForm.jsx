import { useState, useEffect, useContext } from 'react'
import { UserContext } from './UserContext'
import { API_HOST } from '../constants'
import '../App.css'
import InstrumentList from '../InstrumentsList'

const AddInstrumentForm = ({ token }) => {
    const { loggedInUser } = useContext(UserContext)
    const [userData, setUserData] = useState(null)
    const [selectedInstrument, setSelectedInstrument] = useState('')
    const [error, setError] = useState(null)

    useEffect(() => {
        const fetchUserData = async () => {
            try {
                const response = await fetch(
                    `${API_HOST}/users/${loggedInUser}`
                )
                if (!response.ok) {
                    throw new Error(
                        `Failed to fetch user data: ${response.status} ${response.statusText}`
                    )
                }
                const userData = await response.json()
                setUserData(userData)
            } catch (error) {
                console.error('Error fetching user data:', error)
                setError('Failed to fetch user data. Please try again.')
            }
        }

        if (loggedInUser) {
            fetchUserData()
        }
    }, [loggedInUser])

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const response = await fetch(
                `${API_HOST}/users/${loggedInUser}/instruments`,
                {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify([selectedInstrument]),
                }
            )

            if (!response.ok) {
                throw new Error(
                    `Failed to add instrument: ${response.status} ${response.statusText}`
                )
            }
            console.log('Instrument added successfully!')
        } catch (error) {
            console.error('Error adding instrument:', error)
            setError('Failed to add instrument. Please try again.')
        }
    }

    if (!userData) {
        return <div>Loading...</div>
    }

    return (
        <div>
            <h1>Add Instrument</h1>
            {error && <p>{error}</p>}
            <form onSubmit={handleSubmit}>
                <label>Select Instrument:</label>
                <select
                    value={selectedInstrument}
                    onChange={(e) => {
                        setSelectedInstrument(e.target.value)
                        console.log(e.target.value)
                    }}
                >
                    <option value="">-- Select an instrument --</option>
                    {InstrumentList.map((instrument) => (
                        <option key={instrument} value={instrument}>
                            {instrument}
                        </option>
                    ))}
                </select>
                <button type="submit">Add Instrument</button>
            </form>
        </div>
    )
}

export default AddInstrumentForm
