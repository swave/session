import { useState, useEffect, useContext } from 'react'
import '../App.css'
import useToken from '@galvanize-inc/jwtdown-for-react'
import { UserContext } from './UserContext'
import { API_HOST } from '../constants'

function LoginForm() {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const { login } = useToken()
    const { setLoggedInUser } = useContext(UserContext)

    useEffect(() => {
        const loggedInUser = sessionStorage.getItem('loggedInUser')
        if (loggedInUser) {
            setLoggedInUser(loggedInUser)
        }
    }, [setLoggedInUser])

    const handleSubmit = async (e) => {
        e.preventDefault()
        const success = await login(username, password)
        if (success) {
            setLoggedInUser(username)
            sessionStorage.setItem('loggedInUser', username)
            try {
                const response = await fetch(`${API_HOST}/users/${username}`)
                if (response.ok) {
                    const userData = await response.json()
                    console.log('User data:', userData)
                } else {
                    console.error(
                        'Error fetching user data:',
                        response.statusText
                    )
                }
            } catch (error) {
                console.error('Error fetching user data:', error)
            }
        }
        e.target.reset()
    }

    return (
        <div className="flex items-center justify-center h-screen">
            <div className="max-w-md p-6 bg-white rounded shadow-md">
                <h1 className="text-2xl font-bold mb-4">User Login</h1>
                <form onSubmit={(e) => handleSubmit(e)}>
                    <div className="mb-4">
                        <label
                            htmlFor="username"
                            className="block text-gray-600"
                        >
                            Username:
                        </label>
                        <input
                            type="text"
                            name="username"
                            onChange={(e) => setUsername(e.target.value)}
                            required
                            className="w-full p-2 border rounded mt-1"
                        />
                    </div>
                    <div className="mb-4">
                        <label
                            htmlFor="password"
                            className="block text-gray-600"
                        >
                            Password:
                        </label>
                        <input
                            name="password"
                            type="password"
                            id="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                            className="w-full p-2 border rounded mt-1"
                        />
                    </div>
                    <div>
                        <button
                            type="submit"
                            className="w-full bg-blue-500 text-white p-2 rounded"
                        >
                            Login
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default LoginForm
