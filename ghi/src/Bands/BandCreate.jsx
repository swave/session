import { useState } from 'react'
import '../App.css'
import { useSelector } from 'react-redux'
import {
    selectRegistrationLoading,
    selectRegistrationError,
} from '../registrationSlice'
import useToken from '@galvanize-inc/jwtdown-for-react'
import { API_HOST } from '../constants'

const BandCreate = () => {
    const loading = useSelector(selectRegistrationLoading)
    const error = useSelector(selectRegistrationError)
    const { token } = useToken()

    const [photoError, setPhotoError] = useState(false)

    const [formData, setFormData] = useState({
        username: '',
        name: '',
        email: '',
        photo: 'https://cdn.vox-cdn.com/thumbor/r1hD10sng8yqea6hXejrm8o0_wo=/0x0:712x423/1400x1050/filters:focal(385x120:497x232):format(png)/cdn.vox-cdn.com/uploads/chorus_image/image/55531033/Screen_Shot_2017_06_30_at_3.17.00_PM.0.png',
    })

    const handleFormChange = (e) => {
        const value = e.target.value
        const inputID = e.target.id
        setFormData({
            ...formData,
            [inputID]: value,
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()

        try {
            const BandsURL = `${API_HOST}/bands`
            const fetchConfig = {
                method: 'POST',
                body: JSON.stringify(formData),
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
            }

            const response = await fetch(BandsURL, fetchConfig)

            if (response.ok) {
                console.log('Band successfully registered!')
                setFormData({
                    username: '',
                    name: '',
                    email: '',
                    photo: 'https://cdn.vox-cdn.com/thumbor/r1hD10sng8yqea6hXejrm8o0_wo=/0x0:712x423/1400x1050/filters:focal(385x120:497x232):format(png)/cdn.vox-cdn.com/uploads/chorus_image/image/55531033/Screen_Shot_2017_06_30_at_3.17.00_PM.0.png',
                })
            } else {
                const errorMessage = await response.text()
                console.error('Failed to register band:', errorMessage)
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error)
        }
    }

    return (
        <div className="flex items-center justify-center h-screen">
            <div className="max-w-md p-6 bg-white rounded shadow-md text-center">
                <h1 className=" text-3xl mb-4 font-bold">Register Band</h1>
                <form onSubmit={handleSubmit}>
                    <div className="mb-4">
                        <label
                            htmlFor="username"
                            className="block text-gray-600"
                        >
                            Band Username:
                        </label>
                        <input
                            type="text"
                            id="username"
                            value={formData.username}
                            onChange={handleFormChange}
                            placeholder="Band Username"
                            required
                            className="w-full p-2 border rounded mt-1"
                        />
                    </div>
                    <div className="mb-4">
                        <label htmlFor="name" className="block text-gray-600">
                            Name of Band:
                        </label>
                        <input
                            type="text"
                            id="name"
                            value={formData.name}
                            onChange={handleFormChange}
                            placeholder="What is the name of your band?"
                            required
                            className="w-full p-2 border rounded mt-1"
                        />
                    </div>
                    <div className="mb-4">
                        <label htmlFor="email" className="block text-gray-600">
                            Email:
                        </label>
                        <input
                            type="text"
                            id="email"
                            value={formData.email}
                            onChange={handleFormChange}
                            placeholder="email@email.com"
                            required
                            className="w-full p-2 border rounded mt-1"
                        />
                    </div>
                    <div className="mb-4">
                        <label htmlFor="photo" className="block text-gray-600">
                            Photo:
                        </label>
                        <input
                            type="text"
                            id="photo"
                            value={formData.photo}
                            onChange={handleFormChange}
                            placeholder="Enter a valid image URL"
                            className="w-full p-2 border rounded mt-1"
                        />
                        {photoError && (
                            <p className="text-red-500 mt-1">
                                Invalid photo URL. Please enter a valid image
                                URL.
                            </p>
                        )}
                    </div>
                    <img
                        src={formData.photo}
                        alt="Event photo"
                        onError={() => {
                            setPhotoError(true)
                        }}
                        className="mt-4 rounded"
                    />
                    {error && <p className="text-red-500 mb-4">{error}</p>}
                    <div>
                        <button
                            type="submit"
                            className="w-full bg-blue-500 text-white p-2 rounded"
                            disabled={loading}
                        >
                            {loading ? 'Registering Band...' : 'Register Band'}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default BandCreate
