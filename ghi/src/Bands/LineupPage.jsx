import { useState, useEffect } from 'react'
import { API_HOST } from '../constants'
import { NavLink } from 'react-router-dom'
import '../App.css'

function Lineup() {
    const [bands, setBands] = useState([])
    const [searchTerm, setSearchTerm] = useState('')

    useEffect(() => {
        fetch(`${API_HOST}/bands`)
            .then((response) => response.json())
            .then((data) => setBands(data))
            .catch((error) => console.error('Error fetching bands:', error))
    }, [])

    if (bands.length === 0) {
        return <div>Loading Bands...</div>
    }

    const filteredBands = bands.filter((band) =>
        searchTerm
            .toLowerCase()
            .split(' ')
            .every(
                (term) =>
                    band.username.toLowerCase().includes(term) ||
                    band.name.toLowerCase().includes(term)
            )
    )

    return (
        <div className="container mx-auto mt-8 flex justify-center">
            <div className="w-3/4 p-4 flex flex-wrap">
                <h1 className="text-3xl font-semibold mb-4 w-full">
                    Lineup - Session Bands:
                </h1>
                    <input
                        type="text"
                        placeholder="Search Bands"
                        value={searchTerm}
                        onChange={(e) => setSearchTerm(e.target.value)}
                        className="w-full p-2 border bg-slate-900 border-slate-800 text-white rounded"
                    />
                <div className="w-3/4 p-4 flex flex-wrap">
                    <h1 className="text-3xl font-semibold mb-4 w-full">
                        Bands Lineup
                    </h1>
                    {filteredBands.map((band) => (
                        <NavLink
                            key={band.id}
                            to={`/bands/${band.id}/`}
                            className="w-1/3 px-2 py-2 mb-4 my-2"
                        >
                            <div>
                                <img
                                    src={band.photo}
                                    alt="Profile"
                                    className="w-full h-40 object-cover rounded-t"
                                />
                                <div className="bg-white rounded shadow-xl">
                                    <h2 className="text-xl bg-green-900 font-semibold text-white px-2 py-2">
                                        {band.name}
                                    </h2>
                                </div>
                            </div>
                        </NavLink>
                    ))}
                    {filteredBands.length === 0 && (
                        <p className="text-gray-600 w-full">No bands found.</p>
                    )}
                </div>
            </div>
        </div>
    )
}

export default Lineup
