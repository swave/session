import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { API_HOST } from '../constants'
import useToken from '@galvanize-inc/jwtdown-for-react'

function BandProfile() {
    const [band, setBand] = useState(null)
    const { token } = useToken()
    const { band_id } = useParams() // Retrieve band_id from URL params

    useEffect(() => {
        // Fetch band data from the backend
        fetch(`${API_HOST}/bands/${band_id}`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`, // You need to obtain token from somewhere
            },
        })
            .then((response) => response.json())
            .then((data) => setBand(data))
            .catch((error) => console.error('Error fetching band:', error))
    }, [band_id, token]) // Make sure to include band_id and token in the dependency array

    if (!band) {
        return <div>Loading...</div>
    }

    return (
        <div className="flex justify-center items-center h-screen">
            <div className="max-w-800 bg-white p-8 rounded shadow-md">
                {/* Profile Details */}
                <div className="mb-8">
                    <h1 className="text-3xl font-semibold mb-4">{band.name}</h1>

                    {/* Band Picture */}
                    <div className="mb-4">
                        <img
                            src={band.photo}
                            alt="Profile Photo"
                            className="w-24 h-24 object-cover rounded-full"
                        />
                    </div>

                    {/* Band Username */}
                    <div className="mb-4">
                        <p className="font-semibold">Username:</p>
                        <p>{band.username}</p>
                    </div>

                    {/* Email */}
                    <div className="mb-4">
                        <p className="font-semibold">Email:</p>
                        <p>{band.email}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default BandProfile
