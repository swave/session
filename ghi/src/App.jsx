// This makes VSCode check types as if you are using TypeScript
// @ ts-check
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import MainPage from './Users/JamPage'
import Nav from './Nav'
import LoginForm from './Users/LoginForm'
import SignUpForm from './Users/SignUpForm'
import './App.css'
import BandProfile from './Bands/BandProfilePage'
import Lineup from './Bands/LineupPage'
import VenueCreate from './Venues/VenueCreate'
import VenueProfile from './Venues/VenueProfilePage'
import Venues from './Venues/VenuesPage'
import UserDetailPage from './Users/JamUserDetails'
import { AuthProvider } from '@galvanize-inc/jwtdown-for-react'
import MyProfilePage from './Users/MyProfilePage'
import { UserProvider } from './Users/UserContext'
import { API_HOST } from './constants'
import InstrumentForm from './Users/AddInstrumentForm'
import GenreForm from './Users/AddGenreForm'
import BandCreate from './Bands/BandCreate'
//logged in user context component ()

// All your environment variables in vite are in this object
console.table(import.meta.env)

// When using environment variables, you should do a check to see if
// they are defined or not and throw an appropriate error message

// const API_HOST = import.meta.env.VITE_API_HOST
// console.log('API_HOST:', API_HOST)

if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined')
}

function App() {
    return (
        <AuthProvider baseUrl={API_HOST}>
            <UserProvider>
                <BrowserRouter>
                    <Nav />
                    <div className="container">
                        <Routes>
                            <Route path="/" element={<MainPage />} />
                            <Route
                                path="/users/login/"
                                element={<LoginForm />}
                            />
                            <Route
                                path="/users/signup/"
                                element={<SignUpForm />}
                            />
                            <Route
                                path="/bands/:band_id/"
                                element={<BandProfile />}
                            />
                            <Route path="/bands/" element={<Lineup />} />
                            <Route
                                path="/venues/register/"
                                element={<VenueCreate />}
                            />
                            <Route
                                path="/venues/:id/"
                                element={<VenueProfile />}
                            />
                            <Route path="/venues/" element={<Venues />} />
                            <Route
                                path="/users/myprofile/"
                                element={<MyProfilePage />}
                            />
                            <Route
                                path="/users/profile/:username/"
                                element={<UserDetailPage />}
                            />
                            <Route
                                path="/users/myprofile/instruments/"
                                element={<InstrumentForm />}
                            />
                            <Route
                                path="/users/myprofile/genres/"
                                element={<GenreForm />}
                            />
                            <Route
                                path="/bands/register/"
                                element={<BandCreate />}
                            />
                        </Routes>
                    </div>
                </BrowserRouter>
            </UserProvider>
        </AuthProvider>
    )
}

export default App
