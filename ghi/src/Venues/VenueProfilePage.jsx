import { useState, useEffect } from 'react';
import '../App.css';
import { useParams } from 'react-router-dom';
import { API_HOST } from '../constants';

const UserDetailPage = () => {
    //console.log(useParams);
    const { id } = useParams();
    const [venue, setVenue] = useState(null);
    //const [messages, setMessages] = useState([]);
    console.log("id: ", id);
    useEffect(() => {
        fetch(`${API_HOST}/venues/${id}`)
            .then(response => response.json())
            .then(data => setVenue(data))
            .catch(error => console.error('Error fetching venue:', error));
    }, [id]);

    if (!venue) {
        return <div>Loading...</div>
    }

return (
    <div className="max-w-800 mx-auto bg-white p-8 rounded shadow-md mt-10 flex">
        <div className="w-1/2 mr-5">
            <div className="flex items-center mb-4">
                <img src={venue.photo} alt="Profile" className="w-36 h-36 object-cover rounded-full" />
                <div>
                    <h1 className="text-3xl px-2 font-semibold">
                        {venue.name}
                    </h1>
                    <p className="text-2xl px-2 font-semibold">@{venue.name}</p>
                </div>
            </div>
            <div className="w-full flex justify-between">
                <div className="w-1/2">
                    <h3 className="text-l px-2 font-semibold">City:</h3>
                    <ul className="list-disc list-inside">
                        {venue.city}
                    </ul>
                </div>
                <div className="w-1/2 flex flex-col justify-start">
                    <h3 className="text-l px-2 font-semibold">Capacity:</h3>
                    <ul className="list-disc list-inside">
                        {venue.capacity}
                    </ul>
                </div>
                <div className="w-1/2 flex flex-col justify-start">
                    <h3 className="text-l px-2 font-semibold">Age Limit:</h3>
                    <ul className="list-disc list-inside">
                        {venue.all_ages ? "All Ages Permitted" : "21+ Only"}
                    </ul>
                </div>
            </div>
        </div>
    </div>
);
};

export default UserDetailPage;
