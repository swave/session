import { useState } from 'react';
import '../App.css';
import { useSelector } from 'react-redux';
import { selectRegistrationLoading, selectRegistrationError } from '../registrationSlice';
import useToken from '@galvanize-inc/jwtdown-for-react';
import { API_HOST } from '../constants';

const VenueCreate = () => {
  const loading = useSelector(selectRegistrationLoading);
  const error = useSelector(selectRegistrationError);
  const { token } = useToken();

  const [photoError, setPhotoError] = useState(false);

  const [formData, setFormData] = useState({
    name: '',
    city: '',
    capacity: undefined,
    all_ages: false,
    photo: 'https://t3.ftcdn.net/jpg/02/37/69/04/240_F_237690452_UOFdQtUxYtCkY8fq57MFpGqpfhwwDMpY.jpg',
  });

  const handleFormChange = (e) => {
        const value = e.target.value;
        const inputID = e.target.id;
        setFormData({
            ...formData,
            [inputID]: value,
        });
    };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const VenuesURL = `${API_HOST}/api/venues`;
      const fetchConfig = {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(VenuesURL, fetchConfig);

      if (response.ok) {
        console.log('Venue successfully registered!');
        setFormData({
          name: '',
          city: '',
          capacity: undefined,
          all_ages: false,
          photo: 'https://t3.ftcdn.net/jpg/02/37/69/04/240_F_237690452_UOFdQtUxYtCkY8fq57MFpGqpfhwwDMpY.jpg',
        })
      } else {
        const errorMessage = await response.text();
        console.error('Failed to register venue:', errorMessage);
      }
    } catch (error) {
      console.error('An unexpected error occurred:', error);
    }
  };

  return (
    <div className="flex items-center justify-center h-screen">
      <div className="max-w-md p-6 bg-white rounded shadow-md text-center">
        <h1 className=" text-3xl mb-4 font-bold">Register Venue</h1>
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label htmlFor="name" className="block text-gray-600">Name of Venue:</label>
            <input
              type="text"
              id="name"
              value={formData.name}
              onChange={handleFormChange}
              placeholder="Most Excellent Venue"
              required
              className="w-full p-2 border rounded mt-1"
            />
          </div>
          <div className="mb-4">
            <label htmlFor="city" className="block text-gray-600">City:</label>
            <input
              type="text"
              id="city"
              value={formData.city}
              onChange={handleFormChange}
              placeholder="San Francisco"
              required
              className="w-full p-2 border rounded mt-1"
            />
          </div>
          <div className="mb-4">
            <label htmlFor="capacity" className="block text-gray-600">Capacity:</label>
            <input
              type="number"
              id="capacity"
              value={formData.capacity}
              onChange={handleFormChange}
              placeholder="Enter a number"
              required
              className="w-full p-2 border rounded mt-1"
            />
          </div>
          <div className="mb-4 flex items-center justify-center space-x-4">
            <label htmlFor="all_ages" className="text-gray-600">All ages?</label>
            <input
              type="checkbox"
              id="all_ages"
              checked={formData.all_ages}
              onChange={handleFormChange}
              className="p-2 border rounded mt-1"
            />
          </div>
          <div className="mb-4">
            <label htmlFor="photo" className="block text-gray-600">Photo:</label>
            <input
              type="text"
              id="photo"
              value={formData.photo}
              onChange={handleFormChange}
              placeholder="Enter a valid image URL"
              className="w-full p-2 border rounded mt-1"
            />
            {photoError && (
              <p className="text-red-500 mt-1">Invalid photo URL. Please enter a valid image URL.</p>
            )}
          </div>
            <img
              src={formData.photo}
              alt="Event photo"
              onError={() => {
                setPhotoError(true);
                //setPhoto('https://t3.ftcdn.net/jpg/02/37/69/04/240_F_237690452_UOFdQtUxYtCkY8fq57MFpGqpfhwwDMpY.jpg');
              }}
              className="mt-4 rounded"
            />
          {error && <p className="text-red-500 mb-4">{error}</p>}

          <div>
            <button type="submit" className="w-full bg-blue-500 text-white p-2 rounded" disabled={loading}>
              {loading ? 'Registering Venue...' : 'Register Venue'}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default VenueCreate;
