import { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { API_HOST } from '../constants';

function Venues() {
    const [venues, setVenues] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');

    useEffect(() => {
        fetch(`${API_HOST}/venues`)
            .then(response => response.json())
            .then(data => setVenues(data))
            .catch(error => console.error('Error fetching venues:', error));
    }, []);

    if (venues.length === 0) {
        return <div>Loading Venues...</div>;
    }

    const filteredVenues = venues.filter(venue =>
    searchTerm.toLowerCase().split(" ").every(term =>
        venue.name.toLowerCase().includes(term) ||
        venue.city.toLowerCase().includes(term) ||
        venue.capacity.toString().includes(term)
    )
);

    return (
        <div className="container mx-auto mt-8 flex justify-center">
        <div className="w-3/4 p-4 flex flex-wrap">
            <h1 className="text-3xl font-semibold mb-4 w-full">Jam Session Venues:</h1>
            <input
                type="text"
                placeholder="Name, City, Capacity..."
                value={searchTerm}
                onChange={(e) => setSearchTerm(e.target.value)}
                className="w-full p-2 border bg-slate-900 border-slate-800 text-white rounded"
            />
            {filteredVenues.map(venue => (
                <NavLink key={venue.id} to={`/venues/${venue.id}`} className="w-1/3 px-2 py-2 mb-4 my-2">
                    <div>
                        <img src={venue.photo} alt="Profile" className="w-full h-40 object-cover rounded-t" />
                        <div className="bg-white rounded shadow-xl">
                            <h2 className="text-xl bg-green-900 font-semibold text-white px-2 py-2">{venue.name}</h2>
                    </div>
                </div>
                </NavLink>
            ))}
            {filteredVenues.length === 0 && (
                <p className="text-gray-600 w-full">No venues found.</p>
            )}
        </div>
    </div>
    );
}

export default Venues;
