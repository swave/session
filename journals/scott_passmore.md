## Journals
<!--
- The date of the entry
- A list of features/issues that you worked on and who you worked with, if applicable
- A reflection on any design conversations that you had
- At least one ah-ha! moment that you had during your coding, however small
 -->
22 Mar 2024 -
    Worked on nav bar css/functionality

    Reflection - Fix pipeline from failing unit test

    Ah-ha - Unit test dont always play nice with one another in the pipeline
21 Mar 2024 -
    Worked on unit test

    Reflection - Finish nav bar

    Ah-ha - can force location of of Jam, venues, lineup
20 Mar 2024 -
    T/s deployment

    Reflection - work on nav/bar and unit test

    Ah-ha - having to reset sessionapi for depolyment
19 Mar 2024 -
    T/s deployment

    Reflextion - work on unit test and nav bar

    Ah-ha - styling command for css
18 Mar 2024
    Working on deployment

    Reflextion - finish deployment

    Ah-ha -cnt / uncomments code
15 Mar 2024 -
    Working on deployment

    Reflextion - finish deployment

    Ah-ha -Time is money
14 Mar 2024 -
    Created createband.

    Reflextion -work on deplyment

    Ah-ha - you have to upload a pic to have it on your web site
13 Mar 2024 -
    Tweaked nav bar added and worm pic

    Reflextion - Do deployment

    Ah-ha - git lab pipe line only takes 4 mins
12 Mar 2024 -
    Fixed updating user

    Refelxtion - work on nav bar

    Ah-ha -Teamwork makes the dream work
11 Mar 2024 -
    Edited Delete user

    Reflextion - work on updating user

    Ah-ha - using the correct url the-wiggles in stead of session
08 Mar 2024 -
    Finished try except for bands, venues, users so when a band venues or users doesn't exist you get the correct notification.

    Reflection - Need to clean up the backend code and then merge to main

    Ah-ha - different ways to pass the value you are deleting with dict and tuples
07 Mar 2024 -
    working on try except for bands, venues, users.

    Reflection -  need to finish try and excepts.

    Ah-ha - needed to put you css changes in its own file

06 Mar 2024-
    Finished authentication for login required on bands, venues, users - so you have to be logged in to create, update, and delete bands and venues. Only required on the delete and update for user. Added get one band

    Reflection- Need to work on some error catching, for users delete. I would get ok errors over and over again for deleting users that don't exist.

    Ah-ha when it finally clicked on how to get the authentication required to work on different endpoints.

05 Mar 2024 -
    Finished Authentication for logging in. Added update/delete endpoint in user_queries and user_routers. Backed for Users is completed unless we need to add protected endpoints later down the road. Merged with main. Fixed a problem with flake8 and black not running on my pc had to reinstall then in admin mode.

    Reflection - tomorrow we will work on the front end and finish up bands and venues backend

    Ah-ha - When i learned that the jwt authentication was very specific for the type of input it required in relation to username/email.

04 Mar 2024 -
    Finished the backend Authentication, created authenticator.py. created get_one_user so now you can get one users info. edited our create user to interface with the authenticator correctly.
    edited create_user_table(added hashed_password to it). added get_one_user to our user.py router along with get_token, httperror, accounttoken, accountform, and dependencys to import. Also added to user.py queries useroutwithpassword duplicateusererror, edited error class. added authenticator to the main for routing. Added jwtdown volume, and fastapi signing key, waithost, waitbefore, waittimeout. added jwtdown-fastapi to requirement.txt

    Reflection - Need to add delete and update users endpoints.

    Ah-ha - while running create user fast api is still checking over get_one_user so initially I didn't think I needed to worry about fixing that section until I fixed the create user section. However when it would pull user_data from there it wasn't in a dict() so we kept getting an error for that.

01 Mar 2024 -
    Added queries folder with pool.py and users.py. Added routers folder with users.py, bands.py, venues.py. Got our create user api and list users done. Worked on authentication-creation

    Reflection - building out get and post for user and authentication.

    Ah-ha - Had to use flack8 and black before merging.

29 Feb 2024 -
    Adjusted docker-compose.yaml to have fastapi at the bottom of the services so when we run docker-compose up the fastapi container will run after everything else. Mob coded and Created Users table, bands table, venues table. We migrated the new tables to the data base and then merged it to main.

    Reflection - Since we ended up building the tables todays we will be focusing on assigning task for each of us tomorrow.

    Ah-ha - Learned that Docker Builds tab has dev files that can cause your docker-compose build to not run correctly so if you delete them, your build will work correctly. Making sure there is a comma separating up sql statement and down sql statement so the migration will index it correctly.

28 Feb 2024 -
    Collaboratively coded docker-compose.yaml(Created SQL database, Created pgadmin). Added 2 services and 2 volume.
    Created queries repo under /api with an accounts.py file
    Added psycopg to requirements.txt

    Reflection - We talked about moving forward assigning task for each member

    Ah-ha - Figured out that the ghi: in the docker-compose.yaml is the react part of it.

26 - 27 Feb 2024
    Finished wireframes, api endpoints, mvp. Presented projects to instructors and fixed recommendations.

23 Feb 2024 - cloned session project down, created docker containers, verified web pages worked. Updated wire frame with additional pages, added api to it.
Completed MVP form.
